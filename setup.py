#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import os.path
import warnings

dist_dir = os.path.dirname(os.path.abspath(__file__))
os.system("gunzip -kf %s/ilparser/models/* 2> /dev/null" % (dist_dir))

try:
    from setuptools import setup
    setuptools_available = True
except ImportError:
    from distutils.core import setup
    setuptools_available = False

try:
    # This will create an exe that needs Microsoft Visual C++ 2008
    # Redistributable Package
    import py2exe
except ImportError:
    if len(sys.argv) >= 2 and sys.argv[1] == 'py2exe':
        print("Cannot import py2exe", file=sys.stderr)
        exit(1)

py2exe_options = {
    "bundle_files": 1,
    "compressed": 1,
    "optimize": 2,
    "dist_dir": '.',
    "dll_excludes": ['w9xpopen.exe'],
}

py2exe_console = [{
    "script": "./ilparser/__main__.py",
    "dest_base": "ilparser",
}]

py2exe_params = {
    'console': py2exe_console,
    'options': {"py2exe": py2exe_options},
    'zipfile': None
}

if len(sys.argv) >= 2 and sys.argv[1] == 'py2exe':
    params = py2exe_params
else:
    files_spec = [
        ('etc/bash_completion.d', ['ilparser.bash-completion']),
        ('etc/fish/completions', ['ilparser.fish']),
        ('share/doc/ilparser', ['README.txt']),
        ('share/man/man1', ['ilparser.1'])
    ]
    root = os.path.dirname(os.path.abspath(__file__))
    data_files = []
    for dirname, files in files_spec:
        resfiles = []
        for fn in files:
            if not os.path.exists(fn):
                warnings.warn('Skipping file %s since it is not present. Type  make  to build all automatically generated files.' % fn)
            else:
                resfiles.append(fn)
        data_files.append((dirname, resfiles))

    params = {
        'data_files': data_files,
    }
    if setuptools_available:
        params['entry_points'] = {'console_scripts': ['ilparser = ilparser:main']}
    else:
        params['scripts'] = ['bin/ilparser']


exec(compile(open('ilparser/version.py').read(),
             'ilparser/version.py', 'exec'))

setup(
    name = "ilparser",
    version = __version__,
    description="Parser for Indian Languages",
    long_description = open('README.rst', 'rb').read().decode('utf8'),
    keywords = ['Parser', 'Dependency-Parsing', 'Shift-Redude', 'Arc-Eager',
                'Pseudo-Projectivuty', 'Beam Search', 'NLP'],
    author='Riyaz Ahmad, Irshad Ahmad',
    author_email='riyaz.bhat@research.iiit.ac.in',
    maintainer='Irshad Ahmad',
    maintainer_email='irshad.bhat@research.iiit.ac.in',
    license = "GNU",
    url="https://bitbucket.org/riyazbhat/ilparser",
    packages=['ilparser'],
    package_dir={'ilparser':'ilparser'},
    package_data={'ilparser': ['models/*.npy']}, 

    classifiers=[
        "Topic :: Indian Languages :: Parser",
        "Environment :: Console",
        "License :: Public Domain",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
    ],

    **params
)
