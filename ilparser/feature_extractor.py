#!/usr/bin/python
# Copyright Riyaz Ahmad 2015 - present
#
# This file is part of IL-Parser library.
# 
# IL-Parser library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# IL-Parser Library is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#        GNU General Public License for more details.
# 
#        You should have received a copy of the GNU General Public License
#        along with IL-Parser Library.  If not, see <http://www.gnu.org/licenses/>.
#

# Program for feature extraction given a parser configuration
#
# @author Riyaz Ahmad
#


from collections import defaultdict as dfd, namedtuple as nt

node = nt('leaf',('id','form','lemma','ctag','tag','features','parent','pparent', 'gdrel','pdrel','leftChildren','rightChildren'))
PAD = node(-1, '__PAD__', '__PAD__', '__PAD__', '__PAD__', dfd(str), -1, -1, '_', '_', [], [])

def safeIndexing(array, index):
    try: return array[index]
    except IndexError: return None

class Templates: 
    def HindiBest(self, nodes, stack, i):
        features = list()
        af = features.append

        s0 = nodes[stack[-1]] if stack else PAD
        if s0 is not PAD:
            h = s0.parent
            s0h = nodes[h] if h >= 0 else PAD
            if s0h is not PAD:
                h2 = s0h.parent
                s0h2 = nodes[h2] if h2 >= 0 else PAD
            else:
                s0h2 = PAD
            l = safeIndexing(s0.leftChildren, -1)
            s0l = nodes[l] if l else PAD
            r = safeIndexing(s0.rightChildren, -1)
            s0r = nodes[r] if r else PAD
            l = safeIndexing(s0.leftChildren, -2)
            s0l2 = nodes[l] if l else PAD
            r = safeIndexing(s0.rightChildren, -2)
            s0r2 = nodes[r] if r else PAD
        else:
            s0h = PAD
            s0h2 = PAD
            s0l = PAD
            s0r = PAD
            s0l2 = PAD
            s0r2 = PAD
        n0 = nodes[i] if i < len(nodes) else PAD
        if n0 is not PAD:
            l = safeIndexing(n0.leftChildren, -1)
            n0l = nodes[l] if l else PAD
            l = safeIndexing(n0.leftChildren, -2)
            n0l2 = nodes[l] if l else PAD
        else:
            n0l = PAD
            n0l2 = PAD

        if n0 != PAD and s0 != PAD: d = str(min(n0.id - s0.id,5))
        else: d = "NA"

        n0vl = len(n0.leftChildren)

        n1 = nodes[i+1] if i+1 < len(nodes) else PAD
        n2 = nodes[i+2] if i+2 < len(nodes) else PAD

        s0sr = ":".join(nodes[s0r_].gdrel for s0r_ in s0.rightChildren)
        s0sl = ":".join(nodes[s0l_].gdrel for s0l_ in s0.leftChildren)
        n0sl = ":".join(nodes[n0l_].gdrel for n0l_ in n0.leftChildren)

        n0vl = str(min(len(n0.leftChildren),5)) # valency of b0

        predS0 = s0.id - 1 # previous node of s0 in the linear order of a sentence.
        predS0 = nodes[predS0] if predS0 > 0 else PAD
        #print(predS0)
        s0L,s0r2L = s0.gdrel,s0r2.gdrel

        s0w,n0w,n1w,n0lw,predS0w = s0.form,n0.form,n1.form,n0l.form,predS0.form
        s0p,n0p,n1p,n2p,s0lp,s0rp,n0lp,predS0p = s0.tag,n0.tag,n1.tag,n2.tag,s0l.tag,s0r.tag,n0l.tag,predS0.tag
        s0bp,n0bp,s0rbp = s0.features['chunkId'],n0.features['chunkId'],s0r.features['chunkId']
        s0lm,n0lm,s0rlm = s0.lemma,n0.lemma,s0r.lemma

        s0nm,n0nm,s0rnm = s0.features['num'],n0.features['num'],s0r.features['num']
        s0gn,n0gn,s0rgn = s0.features['gen'],n0.features['gen'],s0r.features['gen']
        s0tam,n0tam,s0rtam = s0.features['tam'],n0.features['tam'],s0r.features['tam']
        s0vib,n0vib,s0rvib = s0.features['vib'],n0.features['vib'],s0r.features['vib']

        #single words
        af('s0w,%s' % (s0w))
        af('n0w,%s' % (n0w))
        af('n1w,%s' % (n1w))
        af('n0lw,%s' % (n0lw))
        af('predS0w,%s' % (predS0w))
        
        af('s0p,%s' % (s0p))
        af('n0p,%s' % (n0p))
        af('n1p,%s' % (n1p))
        af('n2p,%s' % (n2p))
        af('s0lp,%s' % (s0lp))
        af('s0rp,%s' % (s0rp))
        af('n0lp,%s' % (n0lp))
        af('predS0p,%s' % (predS0p))
        
        af('s0lm,%s' % (s0lm))
        af('n0lm,%s' % (n0lm))
        #af('s0rlm,%s' % (s0rlm))

        af('s0bp,%s' % (s0bp))
        af('n0bp,%s' % (n0bp))
        af('s0rbp,%s' % (s0rbp))

        af('s0nm,%s' % (s0nm))
        af('n0nm,%s' % (n0nm))
        af('s0rnm,%s' % (s0rnm))

        af('s0gn,%s' % (s0gn))
        af('n0gn,%s' % (n0gn))
        af('s0rgn,%s' % (s0rgn))

        af('s0tam,%s' % (s0tam))
        af('n0tam,%s' % (n0tam))
        af('s0rtam,%s' % (s0rtam))

        af('s0vib,%s' % (s0vib))
        af('n0vib,%s' % (n0vib))
        af('s0rvib,%s' % (s0rvib))

        #distance
        af("distanceS0n0,%s" % (d))
        
        #valency
        af("valencyn0,%s" % (n0vl))
            
        #unigrams
        af('s0L,%s' % (s0L))
        af('s0r2L,%s' % (s0r2L))

        #labels
        #af('s0sr,%s' % (s0sr))
        #af('s0sl,%s' % (s0sl))
        #af('n0sl,%s' % (n0sl))
        
        ##pairs
        #af("s0p,n0p_%s_%s" % (s0p, n0p))
        #af("s0feats,n0feats_%s_%s" % ("|".join((str(s0bp),str(s0nm),str(s0gn),str(s0tam),str(s0vib))), \
        #                "|".join((str(n0bp),str(n0nm),str(n0gn),str(n0tam),str(n0vib)))))
        #af("s0feats,s0L_%s_%s" % ("|".join((str(s0bp),str(s0nm),str(s0gn),str(s0tam),str(s0vib))), s0L))
        return features

    def ExtendedZhang(self, nodes, stack, i):
        """
        Extends feature template described in Yue Zhang and Joakim Nivre, acl 2011.
        http://www.sutd.edu.sg/cmsresource/faculty/yuezhang/acl11j.pdf
        """
        features = list()
        af = features.append

        s0 = nodes[stack[-1]] if stack else PAD
        if s0 is not PAD:
            h = s0.parent
            s0h = nodes[h] if h >= 0 else PAD
            if s0h is not PAD:
                h2 = s0h.parent
                s0h2 = nodes[h2] if h2 >= 0 else PAD
            else:
                s0h2 = PAD
            l = safeIndexing(s0.leftChildren, -1)
            s0l = nodes[l] if l else PAD
            r = safeIndexing(s0.rightChildren, -1)
            s0r = nodes[r] if r else PAD
            l = safeIndexing(s0.leftChildren, -2)
            s0l2 = nodes[l] if l else PAD
            r = safeIndexing(s0.rightChildren, -2)
            s0r2 = nodes[r] if r else PAD
        else:
            s0h = PAD
            s0h2 = PAD
            s0l = PAD
            s0r = PAD
            s0l2 = PAD
            s0r2 = PAD
        n0 = nodes[i] if i < len(nodes) else PAD
        if n0 is not PAD:
            l = safeIndexing(n0.leftChildren, -1)
            n0l = nodes[l] if l else PAD
            l = safeIndexing(n0.leftChildren, -2)
            n0l2 = nodes[l] if l else PAD
        else:
            n0l = PAD
            n0l2 = PAD

        if n0 != PAD and s0 != PAD: d = str(min(n0.id - s0.id,5))
        else: d = "NA"

        s0vr = len(s0.rightChildren)
        s0vl = len(s0.leftChildren)
        n0vl = len(n0.leftChildren)

        n1 = nodes[i+1] if i+1 < len(nodes) else PAD
        n2 = nodes[i+2] if i+2 < len(nodes) else PAD

        s0sr = ":".join(nodes[s0r_].gdrel for s0r_ in s0.rightChildren)
        s0sl = ":".join(nodes[s0l_].gdrel for s0l_ in s0.leftChildren)
        n0sl = ":".join(nodes[n0l_].gdrel for n0l_ in n0.leftChildren)

        s0L,s0lL,s0rL,n0lL,s0hL,s0l2L,s0r2L,n0l2L = s0.gdrel,s0l.gdrel,s0r.gdrel,n0l.gdrel,s0h.gdrel,s0l2.gdrel,\
            s0r2.gdrel,n0l2.gdrel

        #basic features
        #1. word form
        s0w,n0w,n1w,n2w,s0hw,s0lw,s0rw,n0lw,s0h2w,s0l2w,s0r2w,n0l2w = s0.form,n0.form,n1.form,n2.form,\
            s0h.form,s0l.form,s0r.form,n0l.form,s0h2.form,s0l2.form,s0r2.form,n0l2.form     

        #2. POS tags
        s0p,n0p,n1p,n2p,s0hp,s0lp,s0rp,n0lp,s0h2p,s0l2p,s0r2p,n0l2p = s0.tag,n0.tag,n1.tag,n2.tag,s0h.tag,s0l.tag,\
            s0r.tag,n0l.tag,s0h2.tag,s0l2.tag,s0r2.tag,n0l2.tag
    
        #3. Chunk ids
        s0bp,n0bp,n1bp,n2bp,s0hbp,s0lbp,s0rbp,n0lbp,s0h2bp,s0l2bp,s0r2bp,n0l2bp = s0.features['chunkId'],n0.features['chunkId'],\
            n1.features['chunkId'],n2.features['chunkId'],s0h.features['chunkId'],s0l.features['chunkId'], s0r.features['chunkId'],\
            n0l.features['chunkId'],s0h2.features['chunkId'],s0l2.features['chunkId'],s0r2.features['chunkId'],n0l2.features['chunkId']
        
        #4. lemma
        s0lm,n0lm,n1lm,n2lm,s0hlm,s0llm,s0rlm,n0llm,s0h2lm,s0l2lm,s0r2lm,n0l2lm = s0.lemma,n0.lemma,n1.lemma,n2.lemma,s0h.lemma,\
            s0l.lemma,s0r.lemma,n0l.lemma,s0h2.lemma,s0l2.lemma,s0r2.lemma,n0l2.lemma

        #5. Coarse POS tags
        s0ctag,n0ctag,n1ctag,n2ctag,s0hctag,s0lctag,s0rctag,n0lctag,s0h2ctag,s0l2ctag,s0r2ctag,n0l2ctag = s0.ctag,n0.ctag,\
            n1.ctag,n2.ctag,s0h.ctag,s0l.ctag,s0r.ctag,n0l.ctag,s0h2.ctag,s0l2.ctag,s0r2.ctag,n0l2.ctag

        #6. Grammatical Number
        s0nm,n0nm,n1nm,n2nm,s0hnm,s0lnm,s0rnm,n0lnm,s0h2nm,s0l2nm,s0r2nm,n0l2nm = s0.features['num'],n0.features['num'],\
            n1.features['num'],n2.features['num'],s0h.features['num'],s0l.features['num'],s0r.features['num'],n0l.features['num'],\
            s0h2.features['num'],s0l2.features['num'],s0r2.features['num'],n0l2.features['num']
            
        #7. Grammatical Gender
        s0gn,n0gn,n1gn,n2gn,s0hgn,s0lgn,s0rgn,n0lgn,s0h2gn,s0l2gn,s0r2gn,n0l2gn = s0.features['gen'],n0.features['gen'],\
            n1.features['gen'],n2.features['gen'],s0h.features['gen'],s0l.features['gen'],s0r.features['gen'],n0l.features['gen'],\
            s0h2.features['gen'],s0l2.features['gen'],s0r2.features['gen'],n0l2.features['gen']

        #8. Tense, Aspect, Modality and Case
        s0tam,n0tam,n1tam,n2tam,s0htam,s0ltam,s0rtam,n0ltam,s0h2tam,s0l2tam,s0r2tam,n0l2tam = s0.features['tam'],\
            n0.features['tam'],n1.features['tam'],n2.features['tam'],s0h.features['tam'],s0l.features['tam'],s0r.features['tam'],\
            n0l.features['tam'],s0h2.features['tam'],s0l2.features['tam'],s0r2.features['tam'],n0l2.features['tam']

        #9. Tense, Aspect, Modality and Case
        s0vib,n0vib,n1vib,n2vib,s0hvib,s0lvib,s0rvib,n0lvib,s0h2vib,s0l2vib,s0r2vib,n0l2vib = s0.features['vib'],\
            n0.features['vib'],n1.features['vib'],n2.features['vib'],s0h.features['vib'],s0l.features['vib'],s0r.features['vib'],\
            n0l.features['vib'],s0h2.features['vib'],s0l2.features['vib'],s0r2.features['vib'],n0l2.features['vib']

        #10. Disambiguated Case
        s0psd,n0psd,n1psd,n2psd,s0hpsd,s0lpsd,s0rpsd,n0lpsd,s0h2psd,s0l2psd,s0r2psd,n0l2psd = s0.features['psd'],\
            n0.features['psd'],n1.features['psd'],n2.features['psd'],s0h.features['psd'],s0l.features['psd'],s0r.features['psd'],\
            n0l.features['psd'],s0h2.features['psd'],s0l2.features['psd'],s0r2.features['psd'],n0l2.features['psd']
    
        #11. Complex predication
        s0cp,n0cp,n1cp,n2cp,s0hcp,s0lcp,s0rcp,n0lcp,s0h2cp,s0l2cp,s0r2cp,n0l2cp = s0.features['cp'],n0.features['cp'],\
            n1.features['cp'],n2.features['cp'],s0h.features['cp'],s0l.features['cp'],s0r.features['cp'],n0l.features['cp'],\
            s0h2.features['cp'],s0l2.features['cp'],s0r2.features['cp'],n0l2.features['cp']

        #12. Semantics include: cluster ids, word semantics like animacy, wordnet synsets etc.
        s0sem,n0sem,n1sem,n2sem,s0hsem,s0lsem,s0rsem,n0lsem,s0h2sem,s0l2sem,s0r2sem,n0l2sem = s0.features['sem'],\
            n0.features['sem'],n1.features['sem'],n2.features['sem'],s0h.features['sem'],s0l.features['sem'],s0r.features['sem'],\
            n0l.features['sem'],s0h2.features['sem'],s0l2.features['sem'],s0r2.features['sem'],n0l2.features['sem']
        
        '''af('s0lm,%s' % (s0lm))
        af('n0lm,%s' % (n0lm))
        #af('s0rlm,%s' % (s0rlm))

        af('s0bp,%s' % (s0bp))
        af('n0bp,%s' % (n0bp))
        af('s0rbp,%s' % (s0rbp))

        af('s0nm,%s' % (s0nm))
        af('n0nm,%s' % (n0nm))
        af('s0rnm,%s' % (s0rnm))

        af('s0gn,%s' % (s0gn))
        af('n0gn,%s' % (n0gn))
        af('s0rgn,%s' % (s0rgn))

        af('s0tam,%s' % (s0tam))
        af('n0tam,%s' % (n0tam))
        af('s0rtam,%s' % (s0rtam))

        af('s0vib,%s' % (s0vib))
        af('n0vib,%s' % (n0vib))
        af('s0rvib,%s' % (s0rvib))
        af("s0feats,n0feats_%s_%s" % ("|".join((str(s0bp),str(s0nm),str(s0gn),str(s0tam),str(s0vib))), \
                        "|".join((str(n0bp),str(n0nm),str(n0gn),str(n0tam),str(n0vib)))))
        af("s0feats,s0L_%s_%s" % ("|".join((str(s0bp),str(s0nm),str(s0gn),str(s0tam),str(s0vib))), s0L))''' #from IL template

        af("s0tam_%s" % (s0tam))
        af("n0tam_%s" % (n0tam))
        af("n1tam_%s" % (n1tam))
        af("n2tam_%s" % (n2tam))
        af("s0vib_%s" % (s0vib))
        af("n0vib_%s" % (n0vib))
        af("n1vib_%s" % (n1vib))
        af("n2vib_%s" % (n2vib))

        s0wp = "%s:%s" % (s0w, s0p)
        s0wbp = "%s:%s" % (s0w, s0bp) #chunk
        s0wvib = "%s:%s" % (s0w, s0vib) #vib
        s0wnm = "%s:%s" % (s0w, s0nm) #number 
        s0wgn = "%s:%s" % (s0w, s0gn) #gender
        n0wp = "%s:%s" % (n0w, n0p) 
        n0wbp = "%s:%s" % (n0w, n0bp) #chunk
        n0wvib = "%s:%s" % (n0w, n0vib) #vib
        n0wnm = "%s:%s" % (n0w, n0nm) #number
        n0wgn = "%s:%s" % (n0w, n0gn) #gender
        n1wp = "%s:%s" % (n1w, n0p)
        n1wbp = "%s:%s" % (n1w, n0bp) #chunk
        n1wvib = "%s:%s" % (n1w, n1vib) #vib
        n1wnm = "%s:%s" % (n1w, n1nm) #number
        n1wgn = "%s:%s" % (n1w, n1gn) #gender'''
        n2wp = "%s:%s" % (n2w, n0p)
        n2wbp = "%s:%s" % (n2w, n0bp) #chunk
        n2wvib = "%s:%s" % (n2w, n2vib) #vib
        n2wnm = "%s:%s" % (n2w, n2nm) #number
        n2wgn = "%s:%s" % (n2w, n2gn) #gender'''

        s0lmp = "%s:%s" % (s0lm, s0p) #lemma
        n0lmp = "%s:%s" % (n0lm, n0p) #lemma
        n1lmp = "%s:%s" % (n1lm, n0p) #lemma
        n2lmp = "%s:%s" % (n2lm, n0p) #lemma

        s0lmbp = "%s:%s" % (s0lm, s0bp) #lemma + chunk
        n0lmbp = "%s:%s" % (n0lm, n0bp) #lemma + chunk
        n1lmbp = "%s:%s" % (n1lm, n0bp) #lemma + chunk
        n2lmbp = "%s:%s" % (n2lm, n0bp) #lemma + chunk

        # Single Words NOTE Modifed for lemma, cp, semantics and morph features
        af("s0wp_%s" % (s0wp))
        af("s0wbp_%s" % (s0wbp)) #chunk
        af("s0wnm_%s" % (s0wnm)) #number
        af("s0wgn_%s" % (s0wgn)) #gender'''
        af("s0w_%s"  % (s0w))
        af("s0lm_%s"  % (s0lm)) #lemma
        af("s0lmp_%s"  % (s0lmp)) #lemma + tag
        af("s0lmbp_%s"  % (s0lmbp)) #lemma + chunk
        af("s0p_%s"  % (s0p))
        af("s0bp_%s"  % (s0bp)) #chunk
        af("n0wp_%s" % (n0wp)) 
        af("n0wbp_%s" % (n0wbp)) #chunk
        af("n0wnm_%s" % (n0wnm)) #number
        af("n0wgn_%s" % (n0wgn)) #gender'''
        af("n0w_%s"  % (n0w))
        af("n0lm_%s"  % (n0lm)) #lemma
        af("n0lmp_%s"  % (n0lmp)) #lemma + tag
        af("n0lmbp_%s"  % (n0lmbp)) #lemma + chunk
        af("n0p_%s"  % (n0p))
        af("n0bp_%s"  % (n0bp)) #chunk
        af("n1wp_%s" % (n1wp))
        af("n1wbp_%s" % (n1wbp)) #chunk
        af("n1wnm_%s" % (n1wnm)) #number
        af("n1wgn_%s" % (n1wgn)) #gender'''
        af("n1w_%s"  % (n1w))
        af("n1lm_%s"  % (n1lm)) #lemma
        af("n1lmp_%s"  % (n1lmp)) #lemma + tag
        af("n1lmbp_%s"  % (n1lmbp)) #lemma + chunk
        af("n1p_%s"  % (n1p))
        af("n1bp_%s"  % (n1bp)) #chunk
        af("n2wp_%s" % (n2wp))
        af("n2wbp_%s" % (n2wbp)) #chunk
        af("n2wnm_%s" % (n2wnm)) #number
        af("n2wgn_%s" % (n2wgn)) #gender'''
        af("n2w_%s"  % (n2w))
        af("n2lm_%s"  % (n2lm)) #lemma
        af("n2lmp_%s"  % (n2lmp)) #lemma
        af("n2lmbp_%s"  % (n2lmbp)) #lemma + chunk
        af("n2p_%s"  % (n2p))
        af("n2bp_%s"  % (n2bp)) #chunk
        
        # Pairs
        af("s0wp,n0wp_%s_%s" % (s0wp, n0wp))
        af("s0wbp,n0wbp_%s_%s" % (s0wbp, n0wbp)) #chunk
        af("s0wnm,n0w_%s_%s" % (s0wnm, n0w)) #number
        af("s0wgn,n0w_%s_%s" % (s0wgn, n0w)) #gender
        af("s0w,n0wnm_%s_%s" % (s0w, n0wnm)) #number
        af("s0w,n0wgn_%s_%s" % (s0w, n0wgn)) #gender
        af("s0w,n0vib__%s_%s" % (s0w, n0vib)) #vib
        af("s0w,n0tam__%s_%s" % (s0w, n0tam)) #vib 
        af("s0vib,n0w__%s_%s" % (s0vib, n0w)) #vib
        af("s0vib,n0vib__%s_%s" % (s0vib, n0vib)) #vib
        af("s0vib,n1vib__%s_%s" % (s0vib, n1vib)) #vib
        af("s0vib,n2vib__%s_%s" % (s0vib, n2vib)) #vib
        af("s0tam,n0w__%s_%s" % (s0tam, n0w)) #vib'''
        af("s0lm,n0lm_%s_%s" % (s0lm, n0lm)) #lemma
        af("s0lmp,n0lmp_%s_%s" % (s0lmp, n0lmp)) #lemma
        af("s0lm,n0lmp_%s_%s" % (s0lm, n0lmp)) #lemma
        af("s0lmp,n0lm_%s_%s" % (s0lmp, n0lm)) #lemma
        af("s0lmp,n0p_%s_%s" % (s0lmp, n0p)) #lemma
        af("s0p,n0lmp_%s_%s" % (s0p, n0lmp)) #lemma
        af("s0lmbp,n0lmbp_%s_%s" % (s0lmbp, n0lmbp)) #lemma + chunk
        af("s0lm,n0lmbp_%s_%s" % (s0lm, n0lmbp)) #lemma + chunk
        af("s0lmbp,n0lm_%s_%s" % (s0lmbp, n0lm)) #lemma + chunk
        af("s0lmbp,n0bp_%s_%s" % (s0lmbp, n0bp)) #lemma + chunk
        af("s0bp,n0lmbp_%s_%s" % (s0bp, n0lmbp)) #lemma + chunk
        af("s0wp,n0w_%s_%s" % (s0wp, n0w))
        af("s0wbp,n0w_%s_%s" % (s0wbp, n0w)) #chunk
        af("s0w,n0lm_%s_%s" % (s0w, n0lm)) #lemma
        af("s0lm,n0w_%s_%s" % (s0lm, n0w)) #lemma
        af("s0w,n0wp_%s_%s" % (s0w, n0wp))
        af("s0wp,n0p_%s_%s" % (s0wp, n0p))
        af("s0p,n0wp_%s_%s" % (s0p, n0wp))
        af("s0w,n0wbp_%s_%s" % (s0w, n0wbp)) #chunk
        af("s0wbp,n0bp_%s_%s" % (s0wbp, n0bp)) #chunk
        af("s0bp,n0wbp_%s_%s" % (s0bp, n0wbp)) #chunk
        af("s0w,n0w_%s_%s" % (s0w, n0w)) #?
        af("s0p,n0p_%s_%s" % (s0p, n0p))
        af("n0p,n1p_%s_%s" % (n0p, n1p))
        af("s0bp,n0bp_%s_%s" % (s0bp, n0bp)) #chunk
        af("n0bp,n1bp_%s_%s" % (n0bp, n1bp)) #chunk
        af("n0lm,n1lm_%s_%s" % (n0lm, n1lm)) #lemma
        af("n1lm,n2lm_%s_%s" % (n1lm, n2lm)) #lemma

        # Tuples
        af("n0p,n1p,n2p_%s_%s_%s" % (n0p, n1p, n2p))
        af("n0bp,n1bp,n2bp_%s_%s_%s" % (n0bp, n1bp, n2bp)) #chunk
        af("n0lm,n1lm,n2lm_%s_%s_%s" % (n0lm, n1lm, n2lm)) #lemma
        af("n0vib,n1vib,n2vib_%s_%s_%s" % (n0vib, n1vib, n2vib)) #vib
        af("n0nm,n1nm,n2nm_%s_%s_%s" % (n0nm, n1nm, n2nm)) #number
        af("n0gn,n1gn,n2gn_%s_%s_%s" % (n0gn, n1gn, n2gn)) #gender'''
        af("s0p,n0p,n1p_%s_%s_%s" % (s0p, n0p, n1p))
        af("s0bp,n0bp,n1bp_%s_%s_%s" % (s0bp, n0bp, n1bp)) #chunk
        af("s0vib,n1vib,n2vib_%s_%s_%s" % (s0vib, n0vib, n1vib)) #vib
        af("s0nm,n1nm,n2nm_%s_%s_%s" % (s0nm, n0nm, n1nm)) #number
        af("s0gn,n1gn,n2gn_%s_%s_%s" % (s0gn, n0gn, n1gn)) #gender'''
        af("s0lm,n0lm,n1lm_%s_%s_%s" % (s0lm, n0lm, n1lm)) #lemma
        af("s0hp,s0p,n0p_%s_%s_%s" % (s0hp, s0p, n0p))
        af("s0hbp,s0bp,n0bp_%s_%s_%s" % (s0hbp, s0bp, n0bp)) #chunk
        af("s0hlm,s0lm,n0lm_%s_%s_%s" % (s0hlm, s0lm, n0lm)) #lemma
        af("s0p,s0lp,n0p_%s_%s_%s" % (s0p, s0lp, n0p))
        af("s0bp,s0lbp,n0bp_%s_%s_%s" % (s0bp, s0lbp, n0bp)) #chunk
        af("s0lm,s0llm,n0lm_%s_%s_%s" % (s0lm, s0llm, n0lm)) #lemma
        af("s0p,s0rp,n0p_%s_%s_%s" % (s0p, s0rp, n0p))
        af("s0bp,s0rbp,n0bp_%s_%s_%s" % (s0bp, s0rbp, n0bp)) #chunk
        af("s0lm,s0rlm,n0lm_%s_%s_%s" % (s0lm, s0rlm, n0lm)) #lemma
        af("s0p,n0p,n0lp_%s_%s_%s" % (s0p, n0p, n0lp))
        af("s0bp,n0bp,n0lbp_%s_%s_%s" % (s0bp, n0bp, n0lbp)) #chunk
        af("s0lm,n0lm,n0llm_%s_%s_%s" % (s0lm, n0lm, n0llm)) #lemma

        # Distance
        af("s0wd_%s:%s" % (s0w, d))
        af("s0lmd_%s:%s" % (s0lm, d)) #lemma
        af("s0pd_%s:%s" % (s0p, d))
        af("s0bpd_%s:%s" % (s0bp, d)) #chunk
        af("n0wd_%s:%s" % (n0w, d))
        af("n0lmd_%s:%s" % (n0lm, d)) #lemma
        af("n0pd_%s:%s" % (n0p, d))
        af("n0bpd_%s:%s" % (n0bp, d)) #chunk
        af("s0w,n0w,d_%s:%s:%s" % (s0w, n0w, d))
        af("s0lm,n0lm,d_%s:%s:%s" % (s0lm, n0lm, d)) #lemma
        af("s0p,n0p,d_%s:%s:%s" % (s0p, n0p, d))
        af("s0bp,n0bp,d_%s:%s:%s" % (s0bp, n0bp, d)) #chunk

        # Valence
        af("s0wvr_%s:%s" % (s0w, s0vr))
        af("s0vibvr_%s:%s" % (s0vib, s0vr)) #vib
        af("s0lmvr_%s:%s" % (s0lm, s0vr)) #lemma
        af("s0pvr_%s:%s" % (s0p, s0vr))
        af("s0bpvr_%s:%s" % (s0bp, s0vr)) #chunk
        af("s0wvl_%s:%s" % (s0w, s0vl))
        af("s0vibvl_%s:%s" % (s0vib, s0vl)) #vib
        af("s0lmvl_%s:%s" % (s0lm, s0vl)) #lemma
        af("s0pvl_%s:%s" % (s0p, s0vl))
        af("s0bpvl_%s:%s" % (s0bp, s0vl)) #chunk
        af("n0wvl_%s:%s" % (n0w, n0vl))
        af("n0vibvl_%s:%s" % (n0vib, n0vl)) #vib
        af("n0lmvl_%s:%s" % (n0lm, n0vl)) #lemma
        af("n0pvl_%s:%s" % (n0p, n0vl))
        af("n0bpvl_%s:%s" % (n0bp, n0vl)) #chunk

        # Unigrams
        af("s0hw_%s" % (s0hw))
        af("s0hlm_%s" % (s0hlm)) #lemma
        af("s0hgn_%s" % (s0hgn)) #gender
        af("s0hnm_%s" % (s0hnm)) #number'''

        af("s0lw_%s" % (s0lw))
        af("s0llm_%s" % (s0llm)) #lemma
        af("s0lnm_%s" % (s0lnm)) #number
        af("s0lgn_%s" % (s0lgn)) #gender'''

        af("s0rw_%s" % (s0rw))
        af("s0rlm_%s" % (s0rlm)) #lemma
        af("s0rnm_%s" % (s0rnm)) #number
        af("s0rgn_%s" % (s0rgn)) #gender'''

        af("n0lw_%s" % (n0lw))
        af("n0llm_%s" % (n0llm)) #lemma
        af("n0lnm_%s" % (n0lnm)) #number
        af("n0lgn_%s" % (n0lgn)) #gender'''

        af("s0hp_%s" % (s0hp))
        af("s0hbp_%s" % (s0hbp)) #chunk
        af("s0L_%s" % (s0L))

        af("s0lp_%s" % (s0lp))
        af("s0lbp_%s" % (s0lbp)) #chunk
        af("s0lL_%s" % (s0lL))

        af("s0rp_%s" % (s0rp))
        af("s0rbp_%s" % (s0rbp)) #chunk
        af("s0rL_%s" % (s0rL))

        af("n0lp_%s" % (n0lp))
        af("n0lbp_%s" % (n0lbp)) #chunk
        af("n0lL_%s" % (n0lL))

        # Third-order
        #do we really need the non-grandparent ones?
        af("s0h2lm_%s" % (s0h2lm)) #lemma
        af("s0h2nm_%s" % (s0h2nm)) #number
        af("s0h2gn_%s" % (s0h2gn)) #gender'''

        af("s0l2lm_%s" % (s0l2lm)) #lemma
        af("s0l2nm_%s" % (s0l2nm)) #number
        af("s0l2gn_%s" % (s0l2gn)) #gender'''

        af("s0r2lm_%s" % (s0r2lm)) #lemma
        af("s0r2nm_%s" % (s0r2nm)) #number
        af("s0r2gn_%s" % (s0r2gn)) #gender'''

        af("n0l2nm_%s" % (n0l2nm)) #number
        af("n0l2gn_%s" % (n0l2gn)) #gender'''

        af("s0h2w_%s" % (s0h2w))
        af("s0h2p_%s" % (s0h2p))
        af("s0h2bp_%s" % (s0h2bp)) #chunk
        af("s0hL_%s"  % (s0hL))
        af("s0l2w_%s" % (s0l2w))
        af("s0l2p_%s" % (s0l2p))
        af("s0l2bp_%s" % (s0l2bp)) #chunk
        af("s0l2L_%s" % (s0l2L))
        af("s0r2w_%s" % (s0r2w))
        af("s0r2p_%s" % (s0r2p))
        af("s0r2bp_%s" % (s0r2bp)) #chunk
        af("s0r2L_%s" % (s0r2L))
        af("n0l2w_%s" % (n0l2w))
        af("n0l2p_%s" % (n0l2p))
        af("n0l2bp_%s" % (n0l2bp)) #chunk
        af("n0l2L_%s" % (n0l2L))
        af("s0p,s0lp,s0l2p_%s_%s_%s" % (s0p, s0lp, s0l2p))
        af("s0p,s0rp,s0r2p_%s_%s_%s" % (s0p, s0rp, s0r2p))
        af("s0p,s0hp,s0h2p_%s_%s_%s" % (s0p, s0hp, s0h2p))
        af("n0p,n0lp,n0l2p_%s_%s_%s" % (n0p, n0lp, n0l2p))
        af("s0bp,s0lbp,s0l2bp_%s_%s_%s" % (s0bp, s0lbp, s0l2bp)) #chunk
        af("s0bp,s0rbp,s0r2bp_%s_%s_%s" % (s0bp, s0rbp, s0r2bp)) #chunk
        af("s0bp,s0hbp,s0h2bp_%s_%s_%s" % (s0bp, s0hbp, s0h2bp)) #chunk
        af("n0bp,n0lbp,n0l2bp_%s_%s_%s" % (n0bp, n0lbp, n0l2bp)) #chunk

        # Labels
        af("s0wsr_%s_%s" % (s0w, s0sr))
        af("s0lmsr_%s_%s" % (s0lm, s0sr)) #lemma
        af("s0nmsr_%s_%s" % (s0nm, s0sr)) #number
        af("s0gnsr_%s_%s" % (s0gn, s0sr)) #gender
        af("s0vibsr_%s_%s_%s" % (s0vib,n0vib,s0sr)) #vib
        af("s0psr_%s_%s" % (s0p, s0sr))
        af("s0bpsr_%s_%s" % (s0bp, s0sr)) #chunk
        af("s0wsl_%s_%s" % (s0w, s0sl))
        af("s0nmsl_%s_%s" % (s0nm, s0sl)) #number
        af("s0gnsl_%s_%s" % (s0gn, s0sl)) #gender
        af("s0vibsl_%s_%s_%s" % (s0vib, n0vib, s0sl)) #vib
        af("s0lmsl_%s_%s" % (s0lm, s0sl)) #lemma
        af("s0psl_%s_%s" % (s0p, s0sl))
        af("s0bpsl_%s_%s" % (s0bp, s0sl)) #chunk
        af("n0wsl_%s_%s" % (n0w, n0sl))
        af("n0nmsl_%s_%s" % (n0nm, n0sl)) #number
        af("n0gnsl_%s_%s" % (n0gn, n0sl)) #gender
        af("n0vibsl_%s_%s_%s" % (n0vib,s0vib, n0sl)) #vib
        af("n0s0nmsl_%s_%s_%s" % (n0nm,s0nm, n0sl)) #number
        af("n0s0gnsl_%s_%s_%s" % (n0gn,s0gn, n0sl)) #gender'''
        af("n0lmsl_%s_%s" % (n0lm, n0sl)) #lemma
        af("n0psl_%s_%s" % (n0p, n0sl))
        af("n0bpsl_%s_%s" % (n0bp, n0sl)) #chunk

        return features
