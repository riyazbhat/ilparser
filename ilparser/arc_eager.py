#!/usr/env python
# Copyright Riyaz Ahmad 2015 - present
#
# This file is part of IL-Parser library.
# 
# IL-Parser library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# IL-Parser Library is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#        GNU General Public License for more details.
# 
#        You should have received a copy of the GNU General Public License
#        along with IL-Parser Library.  If not, see <http://www.gnu.org/licenses/>.
#

# Program for dependency parsing  
#
# @author Riyaz Ahmad
#

import numpy as np

class arceager(object):
    
    def SHIFT(self, configuration, label=None):
        """
        Moves the input from buffer to stack.
        """
	b0 = configuration.b0
        configuration.stack.append(b0)
        configuration.b0 = b0+1

    def RIGHTARC(self, configuration, label=None):
        """
        Right reduces the tokens at the buffer and stack. s0 -> b0
        """
	b0 = configuration.b0
        s0 = configuration.stack[-1]
        s0N = configuration.nodes[s0]
        b0N = configuration.nodes[b0]

	configuration.nodes[b0].parent = configuration.nodes[b0].pparent = s0N.id
	configuration.nodes[b0].gdrel = configuration.nodes[b0].pdrel = label
        if b0 < s0:
	    configuration.nodes[s0].leftChildren += [b0]
        else:
	    configuration.nodes[s0].rightChildren += [b0]
        configuration.stack.append(b0)
	configuration.b0 = b0+1

    def LEFTARC(self, configuration, label=None):
        """
        Left reduces the tokens at the stack and buffer. b0 -> s0
        """
	b0 = configuration.b0
        s0 = configuration.stack.pop()
        s0N = configuration.nodes[s0]
        b0N = configuration.nodes[b0]
	configuration.nodes[s0].parent = configuration.nodes[s0].pparent = b0N.id
	configuration.nodes[s0].gdrel = configuration.nodes[s0].pdrel = label
        if s0 < b0:
	    configuration.nodes[b0].leftChildren += [s0]
        else:
	    configuration.nodes[b0].rightChildren += [s0]

    def REDUCE(self, configuration, label=None):
        """
        pops the top of the stack if it has got its head.
        """
	b0 = configuration.b0
        configuration.stack.pop()

    def isFinalState(self, beam):
        """
        Checks if the parser is in final configuration i.e. all the input is 
	consumed and both the stack and queue are empty.
        """
	for configuration in beam:
		b0 = configuration.b0
		# if any configuration is not in terminal state dont stop.
		if not ((len(configuration.stack) == 0) and (len(configuration.nodes[b0:]) == 1)):return False
	return True
	        #return (len(self.stack) == 0) and (len(self.nodes[b0:]) == 1)

    def getValidTransitions(self, configuration):
        moves = {0:self.SHIFT,1:self.LEFTARC,2:self.RIGHTARC,3:self.REDUCE}
	b0 = configuration.b0
        if len(configuration.nodes[b0:])==1:
            del moves[0], moves[2]

        if len(configuration.stack) == 0:
            del moves[1], moves[2], moves[3]
        else:
	    s0 = configuration.stack[-1]
            if (configuration.nodes[b0:]):
		if configuration.nodes[s0].parent == -1: del moves[3]
            if configuration.nodes[s0].parent > -1: del moves[1]
        return moves
