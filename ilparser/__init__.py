#! /usr/bin/env python

import sys
import os.path
import argparse

from .DP import ilparser
from wxconv import WXC

__name__       = "ilparser"
__doc__        = "ilparser: Parser for Indian Languages"
__author__     = ["Riyaz Ahmad", "Irshad Ahmad"]
__version__    = "1.5.0"
__license__    = "GPU"
__maintainer__ = "Irshad Ahmad"
__email__      = ["riyaz.bhat@research.iiit.ac.in", "irshad.bhat@research.iiit.ac.in"]
__all__        = ["ilparser", "pseudo_projectivity", "feature_extractor", "DP"]

def main():
    # parse command line arguments 
    format_list = ['text', 'conll']
    languages = ['ben', 'hin', 'kas', 'tel', 'urd']
    format_help  = "select output format [text|conll]"
    lang_help = "<%s>" % ("|".join(languages))
    parser = argparse.ArgumentParser(prog="ilparser", description="Parser for Indian Languages")
    parser.add_argument('--v', action="version", version="%(prog)s 1.0")
    parser.add_argument('--input', metavar='input', dest="INFILE", type=argparse.FileType('r'), default=sys.stdin, help="<input-file>")
    parser.add_argument('--output', metavar='output', dest="OUTFILE", type=argparse.FileType('w'), default=sys.stdout, help="<output-file>")
    parser.add_argument('--lang', metavar='language', dest="lang", default='hin', choices=languages, help=lang_help)
    parser.add_argument('--format', metavar='format', dest="format_", choices=format_list, default="text", help="%s" %format_help)
    parser.add_argument('--plot', metavar='plot', dest="plot", choices=['utf', 'wx', False], default=False, 
                                                    help="specify to plot output parse trees in [utf|wx]")
    parser.add_argument('--dir', metavar='directory', dest="out_dir", help="directory for plotted parse trees")
    parser.add_argument('--beams', type=int, default=1, dest="beams", help="number for beams for beam search decoding")
    args = parser.parse_args()
    
    sentences = args.INFILE.read().split('\n\n') if args.format_=='conll' else args.INFILE.read().split('\n')
    args.INFILE.close()

    sys.stderr.write("Loading Models ....\n\n")
    convertor = WXC(order='wx2utf', lang=args.lang, format_='conll')
    conll = args.format_=='conll'
    parser = ilparser(out_dir=args.out_dir,
                      plot=args.plot,
                      conll=conll, 
                      script_convertor=convertor, 
                      beamWidth=args.beams, 
                      lang=args.lang)
    sys.stderr.write("Parsing ....\n\n")

    for sentence in sentences:
	if not sentence.strip():continue
	out_parse = parser.getParse([sentence], sflag=False)
	if out_parse:
        	args.OUTFILE.write("%s\n\n" %(out_parse[0]))
    args.OUTFILE.close()
