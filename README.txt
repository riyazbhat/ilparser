======================
ilparser
======================

Parser for Indian Languages

Installation
============

Dependencies
~~~~~~~~~~~~

1. Numpy
2. graphviz-2.36.0 (http://pkgs.fedoraproject.org/repo/pkgs/graphviz/graphviz-2.38.0.tar.gz/5b6a829b2ac94efcd5fa3c223ed6d3ae/graphviz-2.38.0.tar.gz)
3. pydot-1.0.28 (http://pydot.googlecode.com/files/pydot-1.0.28.tar.gz)
4. urllib3 (https://github.com/shazow/urllib3)
5. Wx converter (https://github.com/irshadbhat/python-converter-indic)

To install the dependencies do something like this (Ubuntu):

::

    sudo apt-get install python-numpy
    Follow the installation instruction in INTALL file for pydot and graphviz packages.

Install
~~~~~~~

::

    cd ilparser
    sudo python setup.py install


Example
~~~~~~~

::

    >>> from ilparser import ilparser
    >>> with open('tests/sample.conll') as fp:
    ...   sentences = fp.read()
    ... 
    >>> parser = ilparser(out_dir="output-trees", plot=True)
    >>> #plot is a flag to be set if you want to plot output parse trees
    ... #if plot is True, you need to pass the output directory for plotted trees in "out_dir"
    ... #default plot directory is /home/user/output-trees
    ... #if the specified plot directory already exists it will be cleaned first before redirecting plots to it
    ... #make sure the specified plot directory doesn't contain any important files 
    ...
    >>> parsed_sents = parser.getParse(sentences)
    >>> print(parsed_sents)

Contact
~~~~~~~

::

    Riyaz Ahmad Bhat
    PHD-CL IIITH, Hyderabad
    riyaz.bhat@research.iiit.ac.in
